# Change Log

## [1.0.4](https://koolops.com.mx) (2018-05-12)

**Improvements**
- Fixes on README

## [1.0.3](https://koolops.com.mx) (2018-05-12)

**Improvements**
- Homologacion construiccion packer para ubuntu 16.04
- Se agrega documentacion para comunidad.

## [1.0.2](https://koolops.com.mx) (2018-05-12)

**Improvements**
- Limpieza build files
- Se quita compatilibdad rhel6 y derivados
- Se quita compatilibdad ubuntu14.04 y derivados

## [0.2.0](https://koolops.com.mx) (2017-09-03)

**New**
- CentOS 7.3 64 bit. (vagrant/qemu)

**Improvements**
- Build new version 0.2.0
- New ssh-root-key.sh.
- More homologation between Ubuntu and OracleLinux images.

## [0.1.0](https://koolops.com.mx) (2017-09-02)

**New**
- Ubuntu 14.04 64 bit.
- Ubuntu 16.04 64 bit.
- OracleLinux 7.3 64 bit.

**Improvements**
- Configuration template homologation.
- Provisioning shell scripts separated for os.
- New box stage on Makefile for vagrant box.
